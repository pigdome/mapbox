function increaseTime() {
   $("span.timestamp").each(function() {
        var timestamp = $(this).text();
        if( timestamp == 'now' ){timestamp=0};
        var min = parseInt(timestamp)+1;
        $(this).empty();
        $(this).append(min+" min");
    });
}

function date_time()
{
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
    d = date.getDate();
    day = date.getDay();
    days = new Array('วันอาทิตย์', 'วันจันทร์', 'วันอังคาร', 'วันพุธ', 'วันพฤหัสบดี', 'วันศุกร์', 'วันเสาร์');
    h = date.getHours();
    if(h<10)
    {
            h = "0"+h;
    }
    m = date.getMinutes();
    if(m<10)
    {
            m = "0"+m;
    }
    s = date.getSeconds();
    if(s<10)
    {
            s = "0"+s;
    }
    $("#month-day").empty();
    $("#month-day").append(d+" "+months[month]+" "+year);

    $("#time").empty();
    $("#time").append( h+':'+m+':'+s );

    setTimeout('date_time();','1000');
    return true;
}