var map;
var incident_list = [];
var current_autoplay_id;

function init() {
    mapboxgl.accessToken = 'pk.eyJ1IjoicGlnZG9tZSIsImEiOiJjaXJpdnFlcW0wMDBwZmZtNm91ZWg0eXNkIn0.pN-rv5-CObvLclgPQyqIyw';

    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v9',
        center: [100.5520,13.78981],
        zoom: 10
    });

    statistics();
    loadBus(1);

    window.setInterval(function() {
        loadBus(0);
        statistics();
    }, (1000*60*2));

    window.setInterval(function() {
        increaseTime();
        sort_noty();
    }, (1000*60));

    (function ($){
        date_time();
    })(jQuery);
    
    setTimeout( function(){
    	autoPlayMap();
    }  , 3000 );
}