function loadBus( firsttime ) {
	$.getJSON("http://cloud.traffy.in.th/apis/lib/apiScript/getBusIncident/", function(result){
		//console.log(result);
        $.each(result.info.news,function(index,obj) {
            //console.log(obj);
            var time_begin = new Date(obj.starttime);
            var time_end   = new Date(obj.endtime);
            
            var now        = new Date();
            var diff_time  = time_end.getTime() - now.getTime();
            if( obj.type == "ข้อมูลรถประจำทาง" ){diff_time += (1000*60*5)}
            var time_ago   = Math.floor( ( now.getTime() - time_begin.getTime() )/(60*1000) );
            
            if( time_ago < 0 ){return 1;}
            if( ! checkAuthByType(obj.type) ){return 1;}
            if( exist(obj.id) ){return 1;}
            // create marker
            var div = document.createElement('div');
            div.className = "i-box";
            var el  = document.createElement('i');
            el.className = 'fa fa-2x fa-mapbox '+getIconAccType(obj.title);
            el.id = "btn-"+obj.id;

            el.addEventListener('click', function() {
                window.alert(obj.description);
            });
            div.appendChild(el);

            
            var long = obj.location.point ? obj.location.point.longitude : obj.location.endpoint.longitude;
            var lat  = obj.location.point ? obj.location.point.latitude  : obj.location.endpoint.latitude;
            // add marker to map
            var marker = new mapboxgl.Marker(div).setLngLat([long,lat]).addTo(map);
            setTimeout( function(){$("i#btn-"+obj.id).css("opacity","0.5");}  , 10000 );
            setTimeout( function(){marker.remove();}  , diff_time );
             // add notify
            addNotify(obj,diff_time,time_ago);
        });
        if( firsttime )
	  	{
	  		createIncidentList();
	  	}
        else
        {
            if( getAutoPlay() < 0 && zooming() == 0)
            {
                backToHome();
            }
        }
    });
}

function zoom(el) {

	var id = $(el).attr("id");console.log(id);
	var des = $(el).attr("desc");
    var hl = $(el).attr("highlight");
    current_autoplay_id = id;

    $("ul#noty_bottomCenter_layout_container").empty();

	if( $(el).attr("zooming") == 0 )
	{	
        $(el).attr("zooming",'1');
		$("i[id*=btn-]").each(function() {
            $(this).removeClass("zoom");
        });
		$("i#btn-"+id).addClass("zoom");

        
        $("#notification ul li").css("background-color","#FAFAFA");
        $(el).parent().parent().parent().parent().css("background-color","#CFD8DC");

        pauseBtn();
		setAutoPlay(-2);
		setTimeout(function() {
			addNotifyZoom(des);
		},3000);

		map.flyTo({
	        center: [$(el).attr("lng"),$(el).attr("lat")],
	        zoom  : 13,
	       	bearing: Math.floor((Math.random() * 10) + 1) > 5 ? 90 : -90,
	       	pitch:40,
	       	speed:0.8,
	        duration: 6000,
	    });
	}
	else
	{
        $("#notification ul li").css("background-color","#FAFAFA");
        playBtn()
		setAutoPlay(-1);
        $("i[id*=btn-]").each(function() {
            $(this).removeClass("zoom");
        });
		map.flyTo({
        	center: [100.5520,13.78981],
			zoom: 10,
			bearing: 0,
	       	pitch:0,
	       	speed:0.8,
    	});
        $("ul#noty_bottomCenter_layout_container").empty();
        $("div[zooming=1]").each(function() {
            $(this).attr("zooming",'0');
        });
	}
}

function zoom_auto(el) {

	var id = $(el).attr("id");
	var des = $(el).attr("desc");
    var hl = $(el).attr("highlight");

	$("i[id*=btn-]").each(function() {
        $(this).removeClass("zoom");
    });
	$("i#btn-"+id).addClass("zoom");

    current_autoplay_id = id;
    $(el).parent().parent().parent().parent().insertBefore("#notification ul li:first-child");
    $(el).parent().parent().parent().parent().css("background-color","#CED7DB");
    bounce(el);
    pauseBtn();

	map.flyTo({
        center: [$(el).attr("lng"),$(el).attr("lat")],
        zoom  : 13,
       	bearing: Math.floor((Math.random() * 10) + 1) > 5 ? 90 : -90,
       	pitch:40,
       	speed:0.8,
        duration: 6000,
    });

    setTimeout(function() {
        addNotifyZoom_auto(des,10000,id);
    },3000);
}



function createIncidentList() {
	$("div.noty-btn").each(function() {
		incident_list.push( $(this).attr("id") );
	});
}

function clearIncidentList() {
	incident_list = [];
}

function incidentListIsEmpty() {
    return incident_list.length > 0 ? 0 : 1;
}

function autoPlayMap() {
	if( incident_list.length == 0 )
	{
		trigerAutoPlay();
	}

	disableClick();

	if( getAutoPlay() > 0 )
	{
        var incident_id;
        while( true )
        {
            incident_id = incident_list.shift();
            if( $("div.noty-btn#"+incident_id).length > 0 )
            {
                break;
            }
            if( incident_list.length == 0 )
            {
                backToHome();
                break;
            }
        }
		zoom_auto( $("div.noty-btn#"+incident_id) );
	}
}

function cancleAutoPlayMap() {
	$("ul#noty_bottomCenter_layout_container").empty();
	backToHome();
	trigerAutoPlay();
}

function backToHome() {
	map.flyTo({
    	center: [100.5520,13.78981],
		zoom: 10,
		bearing: 0,
       	pitch:0,
       	speed:1,
	});
	
    $("#notification ul li").css("background-color","#FAFAFA");
	$("i[id*=btn-]").each(function() {
        $(this).removeClass("zoom");
    });
    $("div[zooming=1]").each(function() {
        $(this).attr("zooming",'0');
    });
    
    if( getAutoPlay() == -2 )
    {
    	setAutoPlay(-1);
    }
    else
    {
    	trigerAutoPlay();
    }

    if( getAutoPlay() > 0 )
    {
    	clearIncidentList();
    	createIncidentList();
    	autoPlayMap();
    }
    else
    {
    	$("ul#noty_bottomCenter_layout_container").empty();
        enableClick();
        playBtn();
    }
}

function playBtn() {
    var iconElement = document.getElementById('bth');
    var options = {
        from: 'fa-pause',
        to: 'fa-play',
        animation: 'rubberBand'
    };

    iconate(iconElement, options);
    $(".back-to-home img").attr("src","svg/default/play.svg");
}

function pauseBtn() {
    var iconElement = document.getElementById('bth');
    var options = {
        from: 'fa-play',
        to: 'fa-pause',
        animation: 'rubberBand'
    };

    iconate(iconElement, options);
    $(".back-to-home img").attr("src","svg/default/pause.svg");
}

function disableClick() {
    $(".statistics input[type='checkbox']").each(function() {
        $(this).prop("disabled",true);  
    });
    $("#notification ul li").css("pointer-events","none");
}

function enableClick() {
    $(".statistics input[type='checkbox']").each(function() {
        $(this).prop("disabled",false);  
    });
    $("#notification ul li").css("pointer-events","auto");
}

function getAutoPlay() {
	return $("input#autoplay").val();
}

function setAutoPlay(val) {
	$("input#autoplay").val( val );
}

function trigerAutoPlay() {
	$("input#autoplay").val( $("input#autoplay").val()*-1 );
}

function clearMap() {
	$("i.fa-2x").each(function() {
		$(this.remove());
	});
	$("li").each(function() {
		$(this.remove());
	});
}

function zooming() {
    var boolean = 0;
    $("div.noty-btn").each(function() {
        if( $(this).attr("zooming") == 1 )
        {
            boolean = 1;
            return 1;
        }
    });
    return boolean;
}



function statistics() {
    $.getJSON("http://cloud.traffy.in.th/apis/lib/apiScript/getBusIncident/", function(result){
        var traffy   = 0;
        var accident = 0;
        var weather  = 0;
        var bus      = 0;
        $.each(result.info.news,function(index,obj) {
            switch(obj.type)
            {
                case "การจราจร"           : traffy++; break;
                case "การปิด เปิด หรือเลื่อน" : traffy++;break;
                case "เหตุฉุกเฉิน"         : accident++; break;
                case "สภาพอากาศ"          : weather++; break;
                case "ข้อมูลรถประจำทาง"     : bus++;break;
            }
        });
        $("#traffy span.badge").empty().append(traffy);
        $("#accident span.badge").empty().append(accident);
        $("#weather span.badge").empty().append(weather);
        $("#bus span.badge").empty().append(bus);
    });
}