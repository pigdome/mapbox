function addNotifyZoom(des) {
	var n = noty({
		layout : 'bottomCenter',
		type : 'success',
	    text: des,
        animation: {
            open: 'animated bounceIn',
            close: 'animated bounceOut',
            easing: 'swing',
            speed: 500
        },
        callback: {
            afterShow: function() { if( zooming() == 0 ){ $("ul#noty_bottomCenter_layout_container").empty(); }},
        }
	});
}

function addNotifyZoom_auto(des,time,id) {
	var n = noty({
		layout : 'bottomCenter',
		type : 'success',
	    text: des,
	    timeout : time,
        animation: {
            open: 'animated bounceIn',
            close: 'animated bounceOut',
            easing: 'swing',
            speed: 500
        },
	    callback: {
	    	afterShow: function() { if(getAutoPlay() < 0){ $("ul#noty_bottomCenter_layout_container").empty();}},
        	afterClose: function() {
                if( incidentListIsEmpty() )
                {
                    backToHome();
                }
                else
                {
                    autoPlayMap();
                }
                $("div.noty-btn#"+id).parent().parent().parent().parent().css("background-color","#FAFAFA");
            },
    	},
        maxVisible  : 1,
	});
}

function addNotify(obj,diff_time,time_ago)
{
    if($("#" + obj.id).length > 0) {return true;}
    var acc_type = obj.title;
    //var class_acc_type = getClassAccType(acc_type);
    var description = obj.description.toString();
    description = filter_desc(description);
    

    var ta;
    if( time_ago == 0 )
    {
    	ta = "now";
    }
    else
    {
    	ta = time_ago+" min";
    }
    var long = obj.location.point ? obj.location.point.longitude : obj.location.endpoint.longitude;
    var lat  = obj.location.point ? obj.location.point.latitude  : obj.location.endpoint.latitude;
    var highlight = obj.location.point ? obj.location.point.name  : obj.location.endpoint.name;

    var noty_text =  "<div class='noty-btn' id='"+obj.id+"' lat='"+lat+"' lng='"+long+"' zooming='0' onclick=\"zoom(this,1)\" desc='"+description+"' highlight='"+highlight+"'>"+
                        "<div class='row'>"+
                        	"<div class='col-sm-1'>"+
                     			    "<i class=\"fa fa-2x "+getIconAccType(obj.title)+"\"/>"+
                     		"</div>"+
                           	"<div class='col-sm-1'>"+
                     			"<span class=\"label "+getClassType(obj.type)+"\">"+acc_type+"</span>"+
                     		"</div>"+
                     		"<div class='col-sm-8'>"+
                     			"<span class='timestamp'>"+ta+"</span>"+
                     		"</div>"+
                     	"</div>"+
                     	"<div class='row'>"+
                     		"<div class='col-sm-1'>"+
                     			"<img class=\"spacing\" src=\"https://cdn3.iconfinder.com/data/icons/map-markers-2-1/512/danger-512.png\" >"+
                     		"</div>"+
                     		"<div class='col-sm-10'>"+
                     			"<p>"+description+"</p>"+
                     		"</div>"+
                     	"</div>"+
                     "</div>";


    $("div#notification").noty({
            text        : noty_text,
            type        : 'alert',
            dismissQueue: true,
            force		: true,
            closeWith  	: ['backdrop'],
            layout      : 'topCenter',
            maxVisible  : 1000,
            timeout     : diff_time,
            animation: {
                open: 'animated bounceIn',
                close: 'animated bounceOut',
                easing: 'swing',
                speed: 500
            }
    });

}


function sort_noty() {
    /*$("#notification ul li").each(function() {
        if( $(this).children("span.timestamp").text() == "now" )
        {
            $(this).insertBefore( $("#notification ul li:first-child") );   
        }
    });*/
}