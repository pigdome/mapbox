var filter = {};
filter.traffy   = true;
filter.accident = true;
filter.weather  = true;
filter.bus      = false;

function checkAuthByType(type) {
    switch(type)
    {
        case "การจราจร"            : return filter.traffy;
        case "การปิด เปิด หรือเลื่อน"  : return filter.traffy;
        case "เหตุฉุกเฉิน"          : return filter.accident;
        case "สภาพอากาศ"           : return filter.weather;
        case "ข้อมูลรถประจำทาง"      : return filter.bus;
    }
}

function exist(id) {
    if( $("img#btn-"+id).length > 0 )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

function filter_type() {
    filter.traffy   = $("#traffy input[type='checkbox']").prop("checked");
    filter.accident = $("#accident input[type='checkbox']").prop("checked");
    filter.weather  = $("#weather input[type='checkbox']").prop("checked");
    filter.bus      = $("#bus input[type='checkbox']").prop("checked");
    clearMap();
    loadBus(1);
}

function getIconAccType(accType) {

    switch(accType)
    {
        case "การก่อสร้าง"   : return "fa-home building"; break;
        case "จราจรคล่องตัว"  : return "fa-car car-green"; break;
        case "จราจรชะลอตัว"   : return "fa-car car-yellow"; break;
        case "จราจรติดขัด"   : return "fa-car car-orange"; break;
        case "จราจรหนาแน่น"  : return "fa-car car-red"; break;

        case "ปิด"       : return "fa-times"; break;
        case "น้ำท่วม"    : return "fa-tint"; break;
        case "ฝนตก"      : return "fa-cloud"; break;
        case "เพลิงไหม้"   : return "fa-fire"; break;
        case "รถเสีย"     : return "fa-car car-broke"; break;
        case "ระวัง"      : return "fa-exclamation-circle "; break;
        case "หลีกเลี่ยง"   : return "fa-exclamation-triangle"; break;
        case "อุบัติเหตุ"   : return "fa-minus-circle"; break;
        
        case "อยู่ในอู่"   : return "fa-bus bus-green"; break;
        case "วิ่งจี้กัน"   : return "fa-bus bus-yellow"; break;
        case "จอดแช่"      : return "fa-bus bus-orange"; break;
        case "ออกนอกเส้นทาง"   : return "fa-bus bus-red"; break;
        case "วิ่งแซง"      : return "fa-bus bus-red2"; break;

        case "อื่นๆ"        : return "fa-dot-circle-o"; break;
    }
    return "";
}

function getDefaultIconAccType(accType) {

    switch(accType)
    {
        case "การก่อสร้าง"   : return "svg/default/building.svg"; break;
        case "จราจรคล่องตัว"  : return "svg/default/tf.svg"; break;
        case "จราจรชะลอตัว"   : return "svg/default/tf.svg"; break;
        case "จราจรติดขัด"   : return "svg/default/tf.svg"; break;
        case "จราจรหนาแน่น"  : return "svg/default/tf.svg"; break;
        case "ปิด"       : return "svg/default/avoid-close.svg"; break;
        case "น้ำท่วม"    : return "svg/default/flood.svg"; break;
        case "ฝนตก"      : return "svg/default/rain.svg"; break;
        case "เพลิงไหม้"   : return "svg/default/fire.svg"; break;
        case "รถเสีย"     : return "svg/default/car.svg"; break;
        case "ระวัง"      : return "svg/default/beware.svg"; break;
        case "หลีกเลี่ยง"   : return "svg/default/avoid-close.svg"; break;
        case "อุบัติเหตุ"   : return "svg/default/accident.svg"; break;
        
        case "อยู่ในอู่"   : return "svg/default/bus.svg"; break;
        case "วิ่งจี้กัน"   : return "svg/default/bus.svg"; break;
        case "จอดแช่"      : return "svg/default/bus.svg"; break;
        case "ออกนอกเส้นทาง"   : return "svg/default/bus.svg"; break;

        case "อื่นๆ"        : return "svg/etc.svg"; break;
    }
    return "";
}

function getClassType(type) {
    switch(type)
    {
        case "การจราจร"            : return "traffy";
        case "การปิด เปิด หรือเลื่อน"  : return "traffy";
        case "เหตุฉุกเฉิน"          : return "accident";
        case "สภาพอากาศ"           : return "weather";
        case "ข้อมูลรถประจำทาง"      : return "bus";
    }
}

function filter_desc(description) {
    var text  = description;
    text = text.replace("\#", "&nbsp;");
    var out   = "";
    var split = text.split(" ");
    $.each(split,function(index,str) {
        if( str.match(/\@/g) || str.match(/http/g) )
        {
            // do nothing :)
        }
        else
        {
            out += str+" ";
        }
    });
    return out;
}

function highlight(des,highlight) {
    return des.replace(highlight,"<span class='highlight'>"+highlight+"</span>");
}

function bounce(el) {
    var animationName = "bounceIn";
    var animationEnd  = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $(el).addClass('animated ' + animationName).one(animationEnd, function() {
        $(el).removeClass('animated ' + animationName);
    });
}